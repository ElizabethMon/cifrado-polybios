﻿using System;

namespace POLIBYOS
{
    class Program
    {
        static void Main(string[] args)
        {


            //Esta mariz fue hecha con la posicion de las eltras de la t1
            int opn;
            String[,] Alfacifrado = {  //arreglo del alfabeto cifrado
            {"A9","A8","A7","A6","A5"},
            {"B9","B8","B7","B6","B5"},
            {"C9","C8","C7","C6","C5"},
            {"D9","D8","D7","D6","D5"},
            {"E9","E8","E7","E6","E5"},
            {"F9","F8","F7","F6","F5"},
            {"G9","G8","G7","G6","G5"},
            {"H9","H8","H7","H6","H5"}
                                    };
            String[,] Alfabeto = { //arreglo de alfabeto y numeros
            {"A","B","C","D","E"},
            {"F","G","H","I","J"},
            {"K","L","M","N","Ñ"},
            {"O","P","Q","R","S"},
            {"T","U","V","W","X"},
            {"Y","Z","0","1","2"},
            {"3","4","5","6","7"},
            {"8","9"," ",",","."}
                                  };
            //Menu de cifrar o decifar
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("|                    POLIBYOS                  |");
            Console.WriteLine("------------------------------------------------");
            Console.WriteLine("|  Seleccione una de las siguientes opciones:  |");
            Console.WriteLine("|----------------------------------------------|");
            Console.WriteLine("|    Cifrar      (1)                           |");
            Console.WriteLine("|    Descifrar   (2)                           |");
            Console.WriteLine("------------------------------------------------");
            Console.Write("Opcion: ");
            opn = int.Parse(Console.ReadLine());            //Recojo la opcion y lo evaluo en  un case 
            switch (opn)
            {
                case 1:                                     //Opcion 1 para cifrar la palabra
                    String PALABRA;
                    String POSICION = "";
                    Console.Write("Ingrese el mensaje a cifrar: (SOLO SE ADMITE: ALFABETO, NUMERO DE 0-9, ESPACIO Y , . )");
                    PALABRA = Console.ReadLine();       //guardo la palabra 
                    //combierto a mayusculas
                    PALABRA = PALABRA.ToUpper();
                    String[] cifraArray = new String[PALABRA.Length];       //Creo un nuevo arreglo del tamaño de la palabra y la almaceno
                    for (int i = 0; i < PALABRA.Length; i++)
                    {

                        cifraArray[i] = PALABRA[i].ToString();
                    }
                    String[] resul = new String[cifraArray.Length];          //Creo un nuevo arreglo donde almacenare la palabra cifrada
                    int fila = 0;
                    int colum = 0;
                    for (fila = 0; fila <= 7; fila++)                         // recorro filas
                    {
                        for (colum = 0; colum <= 4; colum++)                    //Recorro columnas
                        {
                            for (int i = 0; i < cifraArray.Length; i++)         //recorro el arreglo donde esta mi palabra
                            {

                                if (cifraArray[i].Equals(Alfabeto[fila, colum]))        //Evaluo cuando encuentro una letra en mi alfabeto 
                                {
                                    POSICION = Alfacifrado[fila, colum];                // si esto pasa recorro mi alfabetocifrado y almaceno el simbolo que que esta en la posicion donde encontre la letra
                                    resul[i] = POSICION;
                                }
                            }
                        }
                    }                                                                    //El proceso se repite hasta recorrer todo el arreglo de la palabra
                    Console.WriteLine("Mensaje Cifrado: ");
                    for (int i = 0; i < resul.Length; i++)
                    {
                        Console.Write(resul[i]+"|");                                    //Imprimo el arreglo que ontiene el cifrado
                   
                    }

                    break;
                case 2:
                  
                    Console.Write("AUN NO SE REALIZA ESTE CODIGO");
               
                    break;
            
            }// switch 




   
        }//cierre metodo principal


    }//cierre Clase

}//cierre programa
